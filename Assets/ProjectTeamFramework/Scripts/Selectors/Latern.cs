﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(AudioSource))]
public class Latern : MonoBehaviour
{
    AudioSource audioData;
    GameObject latern, latern1, latern2;
     float timelife = 20f;
     public float maxTimeLife = 100.0f;
     //---------------------------------------------------------
     private float RegenTimer = 0.0f;
     //---------------------------------------------------------
     private const float DecreasePerFrame = 1.0f;
     private const float IncreasePerFrame = 5.0f;
     private const float StaminaTimeToRegen = 3.0f;

    bool active = true;

    // Start is called before the first frame update
    void Start()
    {
         latern = GameObject.Find("Spotlight_cookie1");
         latern1 = GameObject.Find("Spotlight_cookie2");
         latern2 = GameObject.Find("Spotlight_cookie3");
        audioData = this.GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
        if (active == true)
        {
            timelife = Mathf.Clamp(timelife - (DecreasePerFrame * Time.deltaTime), 0.0f, maxTimeLife);
            RegenTimer = 0.0f;
            Debug.Log(timelife.ToString());
        }
        else if (timelife < maxTimeLife)
        {
            if (RegenTimer >= StaminaTimeToRegen)
                timelife = Mathf.Clamp(timelife + (IncreasePerFrame * Time.deltaTime), 0.0f, maxTimeLife);
            else
                RegenTimer += Time.deltaTime;
        }

        if(timelife == 0 && active == true)
        {
            active = false;
            latern.SetActive(false);
            latern1.SetActive(false);
            latern2.SetActive(false);


        }

        if (Input.GetKeyDown("f") && latern.activeSelf == true && latern1.activeSelf == true && latern2.activeSelf == true && active == true)
        {
            latern.SetActive(false);
            latern1.SetActive(false);
            latern2.SetActive(false);
            active = false;
            audioData.Play(0);
            
        }
        else
        {
            if(Input.GetKeyDown("f") && latern.activeSelf == false && latern1.activeSelf == false && latern2.activeSelf == false && active == false)
            {
                latern.SetActive(true);
                latern1.SetActive(true);
                latern2.SetActive(true);
                active = true;
                audioData.Play(0);
            }
        }
    }
}
